\select@language {spanish}
\contentsline {chapter}{\numberline {1}Electricidad}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Cargas puntuales}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Distribuciones cont\IeC {\'\i }nuas de carga}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Ley de Gauss}{7}{section.1.3}
\contentsline {section}{\numberline {1.4}Potencial el\IeC {\'e}ctrico}{10}{section.1.4}
\contentsline {section}{\numberline {1.5}Capacitancia}{11}{section.1.5}
\contentsline {section}{\numberline {1.6}Corriente, resistencia y voltaje}{12}{section.1.6}
\contentsline {section}{\numberline {1.7}Circuitos en corriente continua}{13}{section.1.7}
\contentsline {section}{\numberline {1.8}Circuitos en corriente alterna ($\textsl {Avanzado}$)}{15}{section.1.8}
\contentsline {chapter}{\numberline {2}Magnetismo}{17}{chapter.2}
\contentsline {section}{\numberline {2.1}Campo y fuerza magn\IeC {\'e}tica}{17}{section.2.1}
\contentsline {section}{\numberline {2.2}Fuentes de campo magn\IeC {\'e}tico}{19}{section.2.2}
\contentsline {section}{\numberline {2.3}Ley de Amp\IeC {\`e}re}{20}{section.2.3}
\contentsline {section}{\numberline {2.4}Ley de inducci\IeC {\'o}n de Faraday}{22}{section.2.4}
\contentsline {chapter}{\numberline {3}Ondas electromagn\IeC {\'e}ticas}{23}{chapter.3}
\contentsline {chapter}{\numberline {4}\IeC {\'O}ptica }{24}{chapter.4}
\contentsline {section}{\numberline {4.1}\IeC {\'O}ptica geom\IeC {\'e}trica}{24}{section.4.1}
\contentsline {section}{\numberline {4.2}\IeC {\'O}ptica f\IeC {\'\i }sica ($\textsl {Avanzado}$)}{24}{section.4.2}
\contentsline {chapter}{\numberline {5}Respuestas}{25}{chapter.5}
\contentsline {section}{\numberline {5.1}Electricidad}{25}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Cargas puntuales}{25}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Distribuciones cont\IeC {\'\i }nuas de cara}{26}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Ley de Gauss}{27}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}Potencial el\IeC {\'e}ctrico}{28}{subsection.5.1.4}
\contentsline {subsection}{\numberline {5.1.5}Capacitancia}{29}{subsection.5.1.5}
\contentsline {subsection}{\numberline {5.1.6}Corriente, resistencia y voltaje}{30}{subsection.5.1.6}
\contentsline {subsection}{\numberline {5.1.7}Circuitos en corriente continua}{31}{subsection.5.1.7}
\contentsline {subsection}{\numberline {5.1.8}Circuitos en corriente alterna}{32}{subsection.5.1.8}
\contentsline {section}{\numberline {5.2}Magnetismo}{35}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Campo y fuerza magn\IeC {\'e}tica}{35}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Fuentes de campo magn\IeC {\'e}tico}{36}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Ley de Amp\IeC {\`e}re}{37}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}Ley de inducci\IeC {\'o}n de Faraday}{38}{subsection.5.2.4}
\contentsline {section}{\numberline {5.3}Ondas electromagn\IeC {\'e}ticas}{39}{section.5.3}
\contentsline {section}{\numberline {5.4}\IeC {\'O}ptica}{40}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}\IeC {\'O}ptica geom\IeC {\'e}trica}{41}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}\IeC {\'O}ptica f\IeC {\'\i }sica}{42}{subsection.5.4.2}
